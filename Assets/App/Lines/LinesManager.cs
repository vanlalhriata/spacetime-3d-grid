using System.Collections.Generic;
using UnityEngine;

public class LinesManager : MonoBehaviour
{
    [SerializeField] private LineRenderer linePrefab;

    private List<List<Vector3>> linesSource;
    private List<LineRenderer> lines;

    private static Vector3 ORIGIN = Vector3.zero; // Note: Most code hard-coded for zero origin

    private void OnEnable()
    {
        ParametersManager.OnParametersUpdated += onParametersUpdated;
    }

    private void OnDisable()
    {
        ParametersManager.OnParametersUpdated -= onParametersUpdated;
    }

    private void onParametersUpdated()
    {
        curveLines();
    }

    private void Start()
    {
        generateLines();
        curveLines();
    }

    private void generateLines()
    {
        if (null == linesSource)
        {
            linesSource = LinesSource.GenerateSource();
        }

        var numPoints = ParametersManager.Instance.NumPointsOnLine;
        lines = new List<LineRenderer>();
        foreach (var lineSource in linesSource)
        {
            var line = Instantiate(linePrefab, Vector3.zero, Quaternion.identity, transform);
            line.positionCount = numPoints;
            line.SetPositions(lineSource.ToArray());
            lines.Add(line);
        }
    }

    private void curveLines()
    {
        if (null == lines) return;
        if (linesSource.Count != lines.Count)
        {
            Debug.LogError($"LinesManager: linesSource and lines count don't match.");
        }

        var mass = ParametersManager.Instance.Mass;

        int index = 0;
        foreach (var lineSource in linesSource)
        {
            var newPositions = new List<Vector3>();
            foreach (var originalPosition in lineSource)
            {
                var distance = Vector3.Distance(ORIGIN, originalPosition);
                var force = mass / Mathf.Pow(distance, 2);
                var newDistance = Mathf.Min(distance, force / 2f); // Assuming 1 second of acceleration
                var newPosition = Vector3.MoveTowards(originalPosition, ORIGIN, newDistance);

                newPositions.Add(newPosition);
            }

            lines[index].SetPositions(newPositions.ToArray());

            index++;
        }
    }
}
