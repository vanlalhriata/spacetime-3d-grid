using System.Collections.Generic;
using UnityEngine;

public class LinesSource
{
    public static List<List<Vector3>> GenerateSource()
    {
        var source = new List<List<Vector3>>();
        var numLines = ParametersManager.Instance.NumLinesInDimension;
        var linesDistance = ParametersManager.Instance.DistanceBetweenLines;

        // Generate a 2D plane of points through which lines
        // will be generated for each axis
        for (int i = 0; i < numLines; i++)
        {
            var u = linesDistance * (i - ((numLines - 1) / 2f));
            for (int j = 0; j < numLines; j++)
            {
                var v = linesDistance * (j - ((numLines - 1) / 2f));
                var newSources = generate3AxesLines(u, v);
                source.AddRange(newSources);
            }
        }

        return source;
    }

    // For each axis, take u and v as
    // the point on the other two axes, and generate
    // points for the line along that axis
    private static List<List<Vector3>> generate3AxesLines(float u, float v)
    {
        var numPoints = ParametersManager.Instance.NumPointsOnLine;
        var lineLength = ParametersManager.Instance.NumLinesInDimension
            * ParametersManager.Instance.DistanceBetweenLines;
        var pointsDistance = lineLength / ParametersManager.Instance.NumPointsOnLine;

        var xLines = new List<Vector3>();
        var yLines = new List<Vector3>();
        var zLines = new List<Vector3>();
        for (int i = 0; i < numPoints; i++)
        {
            var w = pointsDistance * (i - ((numPoints - 1) / 2f));
            xLines.Add(new Vector3(w, u, v));
            yLines.Add(new Vector3(v, w, u));
            zLines.Add(new Vector3(u, v, w));
        }

        var result = new List<List<Vector3>>();
        result.Add(xLines);
        result.Add(yLines);
        result.Add(zLines);

        return result;
    }
}
