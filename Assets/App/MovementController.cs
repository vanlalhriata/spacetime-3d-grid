using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class MovementController : MonoBehaviour
{
    [SerializeField] private GameObject cameraObject;

    [SerializeField] private float idlePanSpeed = 5f;
    [SerializeField] private float touchZoomSpeed = 0.01f;
    [SerializeField] private float mouseZoomSpeed = 0.3f;
    [SerializeField] private Vector2 touchPanSpeed = new Vector2(0.2f, 0.3f);
    [SerializeField] private Vector2 mousePanSpeed = new Vector2(0.5f, 0.5f);
    [SerializeField] private float maxZoomDistance = 0.7f;
    [SerializeField] private float maxPitchAngle = 60f;

    private Vector2 lastMousePosition;

    private void Start()
    {
        cameraObject.transform.LookAt(Vector3.zero, Vector3.up);
    }

    private void Update()
    {
        bool isIdle = true;
        if (!UIFocusManager.IsInteractingWithUI)
        {
#if UNITY_WEBGL
            // WebGL always returns Input.touchSupported as true
            isIdle = processMouse();
#else
            if (Input.touchSupported)
            {
                isIdle = processTouch();
            }
            else
            {
                isIdle = processMouse();
            }
#endif
        }
        
        if (isIdle) doIdlePan();
        lastMousePosition = Input.mousePosition;
    }

    private bool processTouch()
    {
        if (Input.touchCount == 0) return true;

        if (Input.touchCount > 1)
        {
            // Zoom
            var touch0 = Input.GetTouch(0);
            var touch1 = Input.GetTouch(1);

            var previousDistance = Vector2.Distance(
                touch0.position - touch0.deltaPosition,
                touch1.position - touch1.deltaPosition);
            var currentDistance = Vector2.Distance(touch0.position, touch1.position);

            var delta = currentDistance - previousDistance;
            doZoom(touchZoomSpeed * delta);
        }

        // Take the average of all touch deltas
        var touchesDelta = Input.touches.Aggregate(Vector2.zero, (a, t) => a + t.deltaPosition, a => a)
            / (float)Input.touchCount;
        doPan(touchPanSpeed * touchesDelta);

        return false;
    }

    private bool processMouse()
    {
        bool isIdle = true;

        if (Input.mouseScrollDelta.y != 0)
        {
            doZoom(mouseZoomSpeed * Input.mouseScrollDelta.y);
        }

        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            var delta = (Vector2)Input.mousePosition - lastMousePosition;
            doPan(mousePanSpeed * delta);
            isIdle = false;
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            isIdle = false;
        }

        return isIdle;
    }

    private void doZoom(float amount)
    {
        // Restrict zoom
        var distance = Mathf.Min(amount, cameraObject.transform.position.magnitude - maxZoomDistance);

        cameraObject.transform.position = 
            Vector3.MoveTowards(cameraObject.transform.position, Vector3.zero, distance);
    }

    private void doPan(Vector2 amount)
    {
        // Inverse y
        amount = new Vector2(amount.x, -amount.y);

        // Pan X
        cameraObject.transform.RotateAround(Vector3.zero, Vector3.up, amount.x);

        // Check if pitch has exceeded maximum. Note that range is [0,360] with 0 = 360
        var localPitch = cameraObject.transform.localRotation.eulerAngles.x;
        var maxPitchAngle2 = 360 - maxPitchAngle;
        bool shouldRestrictY = (localPitch > maxPitchAngle && localPitch < 180 && amount.y > 0)
            || (localPitch < maxPitchAngle2 && localPitch > 180 && amount.y < 0);

        // Pan Y if not restricted
        if (!shouldRestrictY)
        {
            // Get the axis to rotate around
            var pitchAxis = cameraObject.transform.TransformDirection(Vector3.right);
            cameraObject.transform.RotateAround(Vector3.zero, pitchAxis, amount.y);
        }
    }

    private void doIdlePan()
    {
        var angle = idlePanSpeed * Time.deltaTime;
        cameraObject.transform.RotateAround(Vector3.zero, Vector3.up, -angle);
    }
}
