using System;
using UnityEngine;
using UnityEngine.UI;

public class ParametersManager : MonoBehaviour
{
    public float Mass = 1f;
    public int NumPointsOnLine = 9;
    public int NumLinesInDimension = 6;
    public float DistanceBetweenLines = 2f;

    [SerializeField] private Text massTextUI;

    public static ParametersManager Instance { get; private set; }
    public static event Action OnParametersUpdated;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        updateMasstext();
    }

    public void HandleSliderValueChanged(float value)
    {
        Mass = value;
        updateMasstext();
        OnParametersUpdated?.Invoke();
    }

    private void updateMasstext()
    {
        massTextUI.text = $"Mass: {Mass.ToString("0.00")} M";
    }
}
