using UnityEngine;
using UnityEngine.EventSystems;

public class UIFocusManager : MonoBehaviour
{
    public static bool IsInteractingWithUI { get; private set; } = false;

    public void HandleBeginDrag(BaseEventData eventData)
    {
        IsInteractingWithUI = true;
    }

    public void HandleEndDrag(BaseEventData eventData)
    {
        IsInteractingWithUI = false;
    }
}
